import { Routes, RouterModule } from '@angular/router';

//Route for content layout with sidebar, navbar and footer
export const Full_ROUTES: Routes = [
  {
    path: 'changelog',
    loadChildren: () => import('../../changelog/changelog.module').then(m => m.ChangeLogModule)
  },
  {
    path: 'full-layout',
    loadChildren: () => import('../../pages/full-layout-page/full-pages.module').then(m => m.FullPagesModule)
  },
  {
    path: 'configuraciones',
    loadChildren: () => import('../../configurations/configurations.module').then(m => m.ConfigurationsModule)
  },
  {
    path: 'usuarios',
    loadChildren: () => import('../../users/users.module').then(m => m.UsersModule)
  },
  {
    path: 'aviones',
    loadChildren: () => import('../../airplanes/airplanes.module').then(m => m.AirplanesModule)
  }
];
