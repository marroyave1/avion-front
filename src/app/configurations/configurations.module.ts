import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListRatingsComponent } from './rating/list-ratings/list-ratings.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { ConfigurationsRoutingModule } from './configurations-routing.module';
import { FormRatingsComponent } from './rating/form-ratings/form-ratings.component';
import { FormBenefitsComponent } from './benefit/form-benefits/form-benefits.component';
import { ListBenefitsComponent } from './benefit/list-benefits/list-benefits.component';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { ListVehiclesComponent } from './vehicle/list-vehicles/list-vehicles.component';
import { FormVehiclesComponent } from './vehicle/form-vehicles/form-vehicles.component';

@NgModule({
  declarations: [ListRatingsComponent, FormRatingsComponent, FormBenefitsComponent,
    ListBenefitsComponent, ListVehiclesComponent, FormVehiclesComponent],
  imports: [
    CommonModule,
    ConfigurationsRoutingModule,
    NgxDatatableModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModalModule,
    NgSelectModule
  ]
})
export class ConfigurationsModule { }
