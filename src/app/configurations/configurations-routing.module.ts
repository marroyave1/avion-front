import { FormBenefitsComponent } from './benefit/form-benefits/form-benefits.component';
import { FormRatingsComponent } from './rating/form-ratings/form-ratings.component';
import { ListRatingsComponent } from './rating/list-ratings/list-ratings.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListBenefitsComponent } from './benefit/list-benefits/list-benefits.component';
import { ListVehiclesComponent } from './vehicle/list-vehicles/list-vehicles.component';
import { FormVehiclesComponent } from './vehicle/form-vehicles/form-vehicles.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'listar-tarifas',
        component: ListRatingsComponent
      },
      {
        path: 'listar-beneficios',
        component: ListBenefitsComponent
      },
      {
        path: 'listar-vehiculos',
        component: ListVehiclesComponent
      },
      {
        path: 'crear-tarifas',
        component: FormRatingsComponent
      },
      {
        path: 'crear-beneficios',
        component: FormBenefitsComponent
      },
      {
        path: 'crear-vehiculos',
        component: FormVehiclesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationsRoutingModule { }
