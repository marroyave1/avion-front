import { Component, OnInit } from '@angular/core';
import { BackendServiceService } from 'app/shared/backend-service/backend-service.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBenefitsComponent } from '../form-benefits/form-benefits.component';
import Swal from 'sweetalert2'
import { BenefitModel } from 'models/entities/benefit-model';
import { ComunicationModel } from 'models/utils/comunication-model';
import { benefit } from 'constants/api-constants';

@Component({
  selector: 'app-list-benefits',
  templateUrl: './list-benefits.component.html',
  styleUrls: ['./list-benefits.component.scss']
})
export class ListBenefitsComponent implements OnInit {

  beneficios: Array<BenefitModel> = new Array();

  columns = [
    {prop: 'id'},
    {prop: 'nombre'},
    {prop: 'descripcion'},
    {prop: 'idTarifa'}
  ];

  constructor(
    private bakendService: BackendServiceService,
    private ngModal: NgbModal
    ) { }

  ngOnInit() {
    this.getBenefits();
  }

  getBenefits() {
    this.bakendService.getService(benefit.apiGet, { })
    .then(
      (getBenefitResult: any) => {
      this.beneficios = [ ...getBenefitResult.listaRespuesta];
      }
    ).catch(
      (getBenefitError) => {
        console.error(getBenefitError);
      }
    )
  }

  viewBenefit(benefitToView) {
    const modalOpened = this.ngModal.open(FormBenefitsComponent, {
      backdrop: 'static',
      size: 'lg'
    });
    modalOpened.componentInstance.benefitModelInfo = {...benefitToView};
    const comunicationModelInfo: ComunicationModel = new ComunicationModel();
    comunicationModelInfo.mode = 'view';
    modalOpened.componentInstance.comunicationModelInfo = comunicationModelInfo;
  }

  editBenefit(benefitToView) {
    const modalOpened = this.ngModal.open(FormBenefitsComponent, {
      backdrop: 'static',
      size: 'lg'
    });
    modalOpened.componentInstance.benefitModelInfo = {...benefitToView};
    const comunicationModelInfo: ComunicationModel = new ComunicationModel();
    comunicationModelInfo.mode = 'edit';
    modalOpened.componentInstance.comunicationModelInfo = comunicationModelInfo;
    modalOpened.componentInstance.eventEmitter.subscribe(
      (response) => {
        if (response === 'Cool') {
          this.getBenefits();
          modalOpened.dismiss();
        }
      }
    )
  }

  createBenefit() {
    const modalOpened = this.ngModal.open(FormBenefitsComponent, {
      backdrop: 'static',
      size: 'lg'
    });
    const comunicationModelInfo: ComunicationModel = new ComunicationModel();
    comunicationModelInfo.mode = 'create';
    modalOpened.componentInstance.comunicationModelInfo = comunicationModelInfo;
    modalOpened.componentInstance.eventEmitter.subscribe(
      (response) => {
        if (response === 'Cool') {
          this.getBenefits();
          modalOpened.dismiss();
        }
      }
    )
  }

  deleteBenefit(idToDelete) {
    this.bakendService.deleteService(benefit.apiDelete, {
      idBeneficioABorrar: idToDelete
    }).then(
      (deleteBenefitResult: any) => {
        if (deleteBenefitResult.respuesta === 'EXITO') {
          Swal.fire({
            title: 'Super tu servicio',
            text: '¡Que efectividad!',
            icon: 'success',
            confirmButtonText: 'Cool'
          }).then(
            () => {
              this.getBenefits();
            }
          )
        }
      }
    ).catch(
      (deleteBenefitError) => {
        console.error(deleteBenefitError);
      }
    )
  }


}
