import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListBenefitsComponent } from './list-benefits.component';

describe('ListBenefitsComponent', () => {
  let component: ListBenefitsComponent;
  let fixture: ComponentFixture<ListBenefitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListBenefitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListBenefitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
