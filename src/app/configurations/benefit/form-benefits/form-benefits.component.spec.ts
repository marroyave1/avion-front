import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBenefitsComponent } from './form-benefits.component';

describe('FormBenefitsComponent', () => {
  let component: FormBenefitsComponent;
  let fixture: ComponentFixture<FormBenefitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormBenefitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBenefitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
