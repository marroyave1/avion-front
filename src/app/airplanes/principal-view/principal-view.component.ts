import { Component, OnInit, ViewChild } from '@angular/core';
import { TableAirplanesComponent } from 'app/shared-components/table-airplanes/table-airplanes.component';
import { PrincipalControlsComponent } from '../principal-controls/principal-controls.component';
import { SecundaryControlsComponent } from '../secundary-controls/secundary-controls.component';
import { BackendServiceService } from 'app/shared/backend-service/backend-service.service';
import { airplane } from 'constants/api-constants';
import { GenericResponseModel } from 'models/response/generic-response-model';
import { ReportViewComponent } from '../report-view/report-view.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-principal-view',
  templateUrl: './principal-view.component.html',
  styleUrls: ['./principal-view.component.scss']
})
export class PrincipalViewComponent implements OnInit {

  @ViewChild(TableAirplanesComponent, null) tableAirplaneComponent: TableAirplanesComponent;
  @ViewChild(PrincipalControlsComponent, null) principalControlComponent: PrincipalControlsComponent;
  @ViewChild(SecundaryControlsComponent, null) secundaryControlComponent: SecundaryControlsComponent;

  constructor(
    private backendService: BackendServiceService,
    private ngModal: NgbModal
  ) { }

  ngOnInit() {
    this.validateTakeOff();
    this.validateToLand();
    this.isInHome();
  }

  getNewPrincipalDirective(newPrincipalDirective) {
    switch (newPrincipalDirective) {
      case 'start': {
        this.backendService.postService(airplane.apiSave, {}, this.tableAirplaneComponent.createReportModel(9)).then(
          (genericResponse: GenericResponseModel) => {
            if (genericResponse.respuesta === 'EXITO') {
              this.tableAirplaneComponent.moveAirplaneHorizontally();
            }
          }
        ).catch(
          (Error) => {
            console.error(Error);
          }
        );
        break;
      }
      case 'takeOff': {
        this.backendService.postService(airplane.apiSave, {}, this.tableAirplaneComponent.createReportModel(10)).then(
          (genericResponse: GenericResponseModel) => {
            if (genericResponse.respuesta === 'EXITO') {
              this.secundaryControlComponent.isFly = true;
            }
          }
        ).catch(
          (Error) => {
            console.error(Error);
          }
        )
        break
      }
      case 'toLand': {
        this.backendService.postService(airplane.apiSave, {}, this.tableAirplaneComponent.createReportModel(11)).then(
          (genericResponse: GenericResponseModel) => {
            if (genericResponse.respuesta === 'EXITO') {
              this.tableAirplaneComponent.toLand();
            }
          }
        ).catch(
          (Error) => {
            console.error(Error);
          }
        )
        break;
      }
      case 'openReport': {
        window.open("http://localhost:4200/aviones/modal-reporte","_blank");
        break;
      }
    }
  }

  getNewSecundaryDirective(newSecondaryDirective) {
    switch (newSecondaryDirective) {
      case 'Down': {
        this.backendService.postService(airplane.apiSave, {}, this.tableAirplaneComponent.createReportModel(12)).then(
          (genericResponse: GenericResponseModel) => {
            if (genericResponse.respuesta === 'EXITO') {
              this.tableAirplaneComponent.downAirplane();
            }
          }
        ).catch(
          (Error) => {
            console.error(Error);
          }
        )
        break;
      }
      case 'Up': {
        this.backendService.postService(airplane.apiSave, {}, this.tableAirplaneComponent.createReportModel(13)).then(
          (genericResponse: GenericResponseModel) => {
            if (genericResponse.respuesta === 'EXITO') {
              this.tableAirplaneComponent.upAirplane();
            }
          }
        ).catch(
          (Error) => {
            console.error(Error);
          }
        )
        break;
      }
      case 'To': {
        this.backendService.postService(airplane.apiSave, {}, this.tableAirplaneComponent.createReportModel(14)).then(
          (genericResponse: GenericResponseModel) => {
            if (genericResponse.respuesta === 'EXITO') {
              this.tableAirplaneComponent.toAirplane(this.secundaryControlComponent.userAlt);
            }
          }
        ).catch(
          (Error) => {
            console.error(Error);
          }
        )
        break;
      }
      case 'Eject': {
        this.backendService.postService(airplane.apiSave, {}, this.tableAirplaneComponent.createReportModel(15)).then(
          (genericResponse: GenericResponseModel) => {
            if (genericResponse.respuesta === 'EXITO') {
              this.tableAirplaneComponent.peopleFalling
                .push([this.tableAirplaneComponent.airplaneAlt, this.tableAirplaneComponent.airplaneDis]);
            }
          }
        ).catch(
          (Error) => {
            console.error(Error);
          }
        )
        break;
      }
      case 'Change': {
        this.backendService.postService(airplane.apiSave, {}, this.tableAirplaneComponent.createReportModel(16)).then(
          (genericResponse: GenericResponseModel) => {
            if (genericResponse.respuesta === 'EXITO') {
              this.tableAirplaneComponent.lightIsPowerOn = !this.tableAirplaneComponent.lightIsPowerOn;
            }
          }
        ).catch(
          (Error) => {
            console.error(Error);
          }
        )
        break;
      }
    }

  }

  validateTakeOff() {
    if (!this.principalControlComponent.canTakeOff) {
      if (this.tableAirplaneComponent.airplaneDis >= this.tableAirplaneComponent.takeOffDistance) {
        this.principalControlComponent.canTakeOff = true;
      } else {
        setTimeout(() => {
          this.validateTakeOff();
        }, 1000);
      }
    }
  }

  validateToLand() {
    if (!this.principalControlComponent.canToLand) {
      if (this.tableAirplaneComponent.airplaneDis >= this.tableAirplaneComponent.landingDistance) {
        this.principalControlComponent.canToLand = true;
      } else {
        setTimeout(() => {
          this.validateToLand();
        }, 1000);
      }
    }
  }

  isInHome() {
    if (this.tableAirplaneComponent.airplaneDis === this.tableAirplaneComponent.travelDistance - 1) {
      this.principalControlComponent.canViewReport = true;
    } else {
      setTimeout(() => {
        this.isInHome();
      }, 1000);
    }
  }

}
