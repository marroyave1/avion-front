import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-secundary-controls',
  templateUrl: './secundary-controls.component.html',
  styleUrls: ['./secundary-controls.component.scss']
})
export class SecundaryControlsComponent implements OnInit {

  @Output() eventEmitter: EventEmitter<any> = new EventEmitter();

  isFly = false;
  userAlt = 0;

  constructor() { }

  ngOnInit() {
  }

  sendNewDirective(newDirective) {
    this.eventEmitter.emit(newDirective);
  }

}
