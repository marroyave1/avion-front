import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecundaryControlsComponent } from './secundary-controls.component';

describe('SecundaryControlsComponent', () => {
  let component: SecundaryControlsComponent;
  let fixture: ComponentFixture<SecundaryControlsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecundaryControlsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecundaryControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
