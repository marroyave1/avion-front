import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AirplanesRoutingModule } from './airplanes-routing.module';
import { PrincipalViewComponent } from './principal-view/principal-view.component';
import { PrincipalControlsComponent } from './principal-controls/principal-controls.component';
import { SecundaryControlsComponent } from './secundary-controls/secundary-controls.component';
import { SharedComponentsModule } from 'app/shared-components/shared-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReportViewComponent } from './report-view/report-view.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [PrincipalViewComponent, PrincipalControlsComponent, SecundaryControlsComponent, ReportViewComponent],
  imports: [
    CommonModule,
    AirplanesRoutingModule,
    SharedComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgxDatatableModule,
    NgbModalModule
  ],
  exports: [
    PrincipalControlsComponent
  ]
})
export class AirplanesModule { }
