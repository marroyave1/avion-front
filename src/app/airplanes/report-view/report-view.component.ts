import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BackendServiceService } from 'app/shared/backend-service/backend-service.service';
import { GenericResponseModel } from 'models/response/generic-response-model';
import { airplane, valueMaster } from 'constants/api-constants';
import { ValueMasterModel } from 'models/entities/value-master';
import { RegisterModel } from 'models/entities/report-model';

@Component({
  selector: 'app-report-view',
  templateUrl: './report-view.component.html',
  styleUrls: ['./report-view.component.scss']
})
export class ReportViewComponent implements OnInit {

  @ViewChild('dateTemplate', null) dateTemplate: TemplateRef<any>;

  filterReportForm: FormGroup;
  movimentAirplaneType: ValueMasterModel = new ValueMasterModel();
  isSearch = false;
  columns;
  registres: Array<RegisterModel> = new Array();
  toltalElements = 0;
  actualPage = 0;
  movimentTypes: Array<ValueMasterModel> = new Array();


  constructor(
    private backendService: BackendServiceService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.columns = [
      { prop: 'id', name: 'Identificador', width: 100 },
      { prop: 'distancia', name: 'Distancia', width: 100 },
      { prop: 'altura', name: 'Altura', width: 100 },
      { prop: 'fechaRegistro', name: 'Fecha Registro', width: 100, cellTemplate: this.dateTemplate },
      { prop: 'tipoMovimientoAvion.dato', name: 'Tipo Movimiento del Avion', width: 100 }
    ];
    this.filterReportForm = this.formBuilder.group({
      movimentAirplaneType: ['', [Validators.required]]
    });
    this.getRegister();
    this.buildSelect();
  }

  onSubmit() {
    this.isSearch = true;
    this.getRegister();
  }

  getRegister() {
    let params = {};
    if (this.isSearch) {
      params = {
        pageRequest: this.actualPage,
        idTypeRegister: this.movimentAirplaneType.id,
        search: this.isSearch
      };
    } else {
      params = {
        pageRequest: this.actualPage
      };
    }
    this.backendService.getService(airplane.apiGet, params)
    .then((getResponse: GenericResponseModel) => {
      this.registres = getResponse.listaRespuesta
      this.toltalElements = Number(getResponse.variable);
    }).catch(
      (getRegistersError) => {
        console.error(getRegistersError);
      }
    )
  }

  setPage(pageRequest) {
    this.actualPage = pageRequest.offset;
    setTimeout(() => {
      this.getRegister();
    }, 1000);
  }

  buildSelect() {
    this.backendService.getService(valueMaster.apiGetByType,
      {
        'idTipoMaestroValor': 8
      }
    )
      .then((getValueMasterResult: any) => {
        this.movimentTypes = [...getValueMasterResult.listaRespuesta];
      }
      ).catch(
        (getValueMasterError) => {
          console.error(getValueMasterError);
        }
      )
  }

  get validatorForm() { return this.filterReportForm.controls; }

}
