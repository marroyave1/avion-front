import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-principal-controls',
  templateUrl: './principal-controls.component.html',
  styleUrls: ['./principal-controls.component.scss']
})
export class PrincipalControlsComponent implements OnInit {

  @Output() eventEmmitter: EventEmitter<any> = new EventEmitter();

  canTakeOff = false;
  canToLand = false;
  canViewReport = false;

  constructor() {
  }

  ngOnInit() {
  }

  sendNewDirective(newDirective) {
    this.eventEmmitter.emit(newDirective);
  }
}
