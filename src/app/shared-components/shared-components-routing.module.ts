import { ListUsersComponent } from './list-users/list-users.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableAirplanesComponent } from './table-airplanes/table-airplanes.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'listar-usuarios',
        component: ListUsersComponent
      },
      {
        path: 'ver-aviones',
        component: TableAirplanesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SharedComponentsRoutingModule { }
