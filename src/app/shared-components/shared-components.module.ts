import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedComponentsRoutingModule } from './shared-components-routing.module';
import { ListUsersComponent } from './list-users/list-users.component';
import { TableAirplanesComponent } from './table-airplanes/table-airplanes.component';
import { PrincipalViewComponent } from 'app/airplanes/principal-view/principal-view.component';

@NgModule({
  declarations: [ListUsersComponent, TableAirplanesComponent],
  imports: [
    CommonModule,
    SharedComponentsRoutingModule,
    NgxDatatableModule,
  ],
  exports: [
    ListUsersComponent,
    TableAirplanesComponent
  ]
})
export class SharedComponentsModule { }
