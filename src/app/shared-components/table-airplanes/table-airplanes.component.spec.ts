import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableAirplanesComponent } from './table-airplanes.component';

describe('TableAirplanesComponent', () => {
  let component: TableAirplanesComponent;
  let fixture: ComponentFixture<TableAirplanesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableAirplanesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableAirplanesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
