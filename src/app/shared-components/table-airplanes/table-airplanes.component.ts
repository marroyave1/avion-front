import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import Swal from 'sweetalert2';
import { RegisterModel } from 'models/entities/report-model';

@Component({
  selector: 'app-table-airplanes',
  templateUrl: './table-airplanes.component.html',
  styleUrls: ['./table-airplanes.component.scss']
})
export class TableAirplanesComponent implements OnInit {

  arrayDistance = [];
  tableView: Array<any> = new Array();
  airplaneSpeed;
  travelDistance;
  takeOffDistance;
  landingDistance;
  maxMSNM = 10;

  airplaneAlt = this.maxMSNM - 1;
  airplaneDis = 0;

  lightIsPowerOn = false;

  peopleFalling = [];

  constructor() {
    this.travelDistance = 25;
    this.airplaneSpeed = 1;
    this.takeOffDistance = 5;
    this.landingDistance = 20;
    this.buildTableAirplaneView();
  }

  ngOnInit() {
  }

  buildTableAirplaneView() {
    const distanceArray = []
    for (let j = 0; j < this.travelDistance; j++) {
      distanceArray.push(0)
    }
    for (let i = 0; i < this.maxMSNM; i++) {
      this.tableView.push([...distanceArray]);
    }
  }

  shouldPaintAirplane(posAlt, posDis) {
    if (posAlt === this.airplaneAlt && posDis === this.airplaneDis) {
      return true;
    } else {
      return false;
    }
  }

  isPeopleFallingHere(posAlt, posDis) {
    let hasPeopleHere = false;
    this.peopleFalling.forEach(
      (personFall) => {
        if (personFall[0] === posAlt && personFall[1] === posDis) {
          hasPeopleHere = true;
        }
      }
    )
    return hasPeopleHere;
  }

  fallPeople() {
    const indexOfPeopleToRemove = []
    for (let i = 0; i < this.peopleFalling.length; i++) {
      if (this.peopleFalling[i][0] === this.maxMSNM - 1) {
        indexOfPeopleToRemove.push(i);
      } else {
        this.peopleFalling[i][0] += 1;
      }
    }

    indexOfPeopleToRemove.forEach(element => {
      this.peopleFalling.splice(element, 1);
    });

    setTimeout(() => {
      this.fallPeople();
    }, 2000);
  }

  moveAirplaneHorizontally() {
    if (this.airplaneDis === this.travelDistance - 1) {
      Swal.fire({
        title: 'Welcome to a destiny travel',
        text: '¡Enjoy!',
        icon: 'success',
        confirmButtonText: 'Cool'
      }).then(
        () => {
        }
      )
    } else {
      this.tableView[this.airplaneAlt][this.airplaneDis] = 1;
      this.airplaneDis = this.airplaneDis + this.airplaneSpeed;
      setTimeout(() => {
        this.moveAirplaneHorizontally();
      }, 1000);
    }
  }

  toLand() {
    if (this.airplaneAlt !== this.maxMSNM - 1) {
      this.airplaneAlt += 1;
    }
    setTimeout(() => {
      this.toLand()
    }, 1000);
  }

  downAirplane() {
    if (this.airplaneAlt < this.maxMSNM) {
      this.airplaneAlt += 1;
    } else {
      Swal.fire({
        title: '¡Casi chocamos!',
        text: 'Ten cuidado, sabes lo horrible que es morir en un avión',
        icon: 'warning',
        confirmButtonText: '¡Ok!'
      }).then(
        () => {
        }
      )
    }
  }

  upAirplane() {
    if (this.airplaneAlt > 0) {
      this.airplaneAlt -= 1;
    } else {
      Swal.fire({
        title: '¡No Podemos volar más alto!',
        text: 'Ten cuidado, sabes lo horrible que es morir en un avión',
        icon: 'warning',
        confirmButtonText: '¡Esta bien!'
      }).then(
        () => {
        }
      )
    }
  }

  toAirplane(toAirplaneAlt) {
    if (this.airplaneAlt === toAirplaneAlt) {
      Swal.fire({
        title: 'We are ready',
        icon: 'success',
        confirmButtonText: 'Cool'
      }).then(
        () => {
        }
      )
    } else if (this.airplaneAlt > toAirplaneAlt) {
      this.upAirplaneTo(toAirplaneAlt);
    } else {
      this.downAirplaneTo(toAirplaneAlt);
    }
  }

  upAirplaneTo(toAirplaneAlt) {
    console.log(toAirplaneAlt);
    console.log(this.airplaneAlt);
    if (this.airplaneAlt == toAirplaneAlt) {
      Swal.fire({
        title: 'We are ready',
        icon: 'success',
        confirmButtonText: 'Cool'
      }).then(
        () => {
        }
      )
    } else {
      this.airplaneAlt -= 1;
      setTimeout(() => {
        this.upAirplaneTo(toAirplaneAlt);
      }, 1000);
    }
  }

  downAirplaneTo(toAirplaneAlt) {
    if (this.airplaneAlt == toAirplaneAlt) {
      Swal.fire({
        title: 'We are ready',
        icon: 'success',
        confirmButtonText: 'Cool'
      }).then(
        () => {
        }
      )
    } else {
      this.airplaneAlt += 1;
      setTimeout(() => {
        this.downAirplaneTo(toAirplaneAlt);
      }, 1000);
    }
  }

  createReportModel(type) {
    const reportModel: RegisterModel = new RegisterModel();
    reportModel.distancia = this.airplaneDis;
    reportModel.altura = this.airplaneAlt;
    reportModel.tipoMovimientoAvion.id = type;
    return reportModel;
  }

}
