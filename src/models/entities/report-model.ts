import { ValueMasterModel } from './value-master';

export class RegisterModel {
    id: number;
    distancia: number;
    altura: number;
    fechaRegistro: Date;
    tipoMovimientoAvion: {
        id: number;
    };

    constructor() {
        this.id = null;
        this.distancia = null;
        this.altura = null;
        this.fechaRegistro = new Date();
        this.tipoMovimientoAvion = new ValueMasterModel();
    }
}